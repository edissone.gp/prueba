package com.Tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import com.Pages.PageLogin;
import com.Reports.ExtentReport;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestLogin {
	
	private WebDriver driver;
	PageLogin pageLogin;
    ExtentReports report;
    ExtentTest test;
	
	@BeforeEach
	public void setUp() throws Exception {
		pageLogin = new PageLogin(driver);
		driver =pageLogin.chromeDriverConnection();
		pageLogin.link("http://fe.deitech.online/");	
	}
	@AfterEach
	public void tearDown() throws Exception {		
		driver.quit();		
	}

	@Test
	public void test() throws InterruptedException {		
		pageLogin.loginUser();
	}
	@Test
	public void test2() throws InterruptedException {
		pageLogin.loginUserFail();
	}

}
