package com.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.Base.BasePage;
import com.Reports.ExtentReport;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class PageLogin extends BasePage {

	By email = By.name("email");
	By password = By.name("password");
	By bottomLogin = By.xpath("//*[@id=\'root\']/header/nav/a[2]");
	By bottomFinish = By.xpath("//*[@id=\'root\']/main/div/form/button");
	By resultado= By.xpath("//*[@id='root']/header/nav/div/div/div/p[2]");
	By resFail = By.xpath("//*[@id=\'root\']/main/div/form/p[1]");
	
    ExtentReports report;	
    ExtentTest test1;
    ExtentTest test2;
    

	
	public PageLogin(WebDriver driver) {
		// TODO Auto-generated constructor stub
	}
	
	public void loginUser() throws InterruptedException {
		report = ExtentReport.getIntance();	
		test1 = report.startTest("Test Login exitoso");
		oprimir(bottomLogin);
		teclear("prueba6@prueba.com",email);
		teclear("pass123",password);
		test1.log(LogStatus.PASS, "Se ingreso correctamente");
		oprimir(bottomFinish);
		esperaExplicita(resultado,"jose gomez",10);
		obtenerTexto(resultado);
		comparar(resultado,"jose gomez");
		report.endTest(test1);
		
	}
	public void loginUserFail() throws InterruptedException {
		report = ExtentReport.getIntance();
		test2 = report.startTest("Test Login Fail");
		oprimir(bottomLogin);
		teclear("prueba6@prueba.com",email);
		teclear("321546",password);
		oprimir(bottomFinish);
		test2.log(LogStatus.PASS, "Se ingreso correctamente la info");
		esperaExplicita(resFail,"Sus credenciales son inválidas. Por favor, vuelva a intentarlo",10);
		comparar(resFail,"Sus credenciales son inválidas. Por favor, vuelva a intentarlo");
		test2.log(LogStatus.PASS, "Se finaliza la prueba correctamente");
		report.endTest(test2);
		report.flush();
	}
}
