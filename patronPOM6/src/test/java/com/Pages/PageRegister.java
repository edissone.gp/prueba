package com.Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.Base.BasePage;

public class PageRegister extends BasePage {

	By buttonRegister = By.linkText("Crear cuenta");
	By buttonRegister1 = By.partialLinkText("cuenta");
	By name = By.name("firstName");
	By lastName = By.id("lastName");
	By email = By.cssSelector("#email");
	By password = By.xpath("/html/body/div/main/div/form/div[3]/input");
	By rePassword = By.xpath("//*[@id=\'repassword\']");
	By buttomFinish = By.cssSelector("#root > main > div > form > button");
	By result = By.className("txt-gracias");
	String correos;
	
	public PageRegister(WebDriver driver) {
		// TODO Auto-generated constructor stub
	}
	
	
	public void registerUser(){
		/*new WebDriverWait(driver,Duration.ofSeconds(10))
		.until(ExpectedConditions.elementToBeClickable(buttonRegister));*/
		oprimir(buttonRegister);
		teclear("Edisson",name);
		teclear("Gomez",lastName);		
		teclear("edisson@hotmail.com",email);
		teclear("123456",password);
		teclear("123456",rePassword);
		oprimir (buttomFinish);
		esperaExplicita(result,"¡Cuenta registrada con éxito!",10);
		//comparar (result,"¡Cuenta registrada con éxito!");
		
	}

}